var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/demo', { useNewUrlParser: true });
mongoose.connection.on('error', () => console.error('db connection'));

var Schema = mongoose.Schema;

var NoteSchema = new Schema({
  text: String,
  date: Date,
});

var NoteModel = mongoose.model('NoteModel', NoteSchema);

const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json());
app.use(cors());

app.get('/note/list', async (req, res) => {
	const notes = await NoteModel.find({}).lean().exec();
	res.json(notes.map(note => note.text));
});

app.post('/note/create', async (req, res) => {
	await NoteModel.create({
		text: req.body.text,
		date: new Date(),
	});
	
	return res.json({success: true});
});

const port = 3001;
app.listen(port, () => console.log(`listening on ${port}`));
